/*
 * dc.c: implementation of dc using decimal doubles
 *
 * In order to keep code simple, all errors are fatal.
 *   Try to peek/pop an empty stack? Fatal.
 *   Try to add a string and a number? Fatal.
 *   Run out of memory? Fatal.
 *   ...
 *
 */

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdnoreturn.h>

/* stuff that is getting pulled into Number library */
typedef double Number;

void numprint(Number n){ printf("%f", n); }
Number numread(FILE *fp){ double d; fscanf(fp, "%lf", &d); return d; }

/* arithmetic */
Number numadd(Number a, Number b){ return a + b; }
Number numsub(Number a, Number b){ return a - b; }
Number nummul(Number a, Number b){ return a * b; }
Number numdiv(Number a, Number b){ return a / b; }
Number nummod(Number a, Number b){ return fmod(a, b); }
Number numpow(Number a, Number b){ return pow(a, b); }
Number numsqrt(Number a){ return sqrt(a); }

/* comparison */
int numgt(Number a, Number b){ return a >  b; }
int numlt(Number a, Number b){ return a <  b; }
int numeq(Number a, Number b){ return a == b; }
int numge(Number a, Number b){ return a >= b; }
int numle(Number a, Number b){ return a <= b; }
int numne(Number a, Number b){ return a != b; }


/*
 * Token types
 * Single characters represent themselves. e.g. '+'
 */
enum {
	NUM = UCHAR_MAX + 1,
	STR,
	LE, GE, NE, /* !>, !<, != */
	COMMENT,
	FP, /* FILE* used to signify a real file not a string. Not a real Token. */
};

/*
 * Reference counted string. Can hold nul bytes. FILE *fp is used to lex() the
 * macro so we don't have to differentiate between files and strings.
 */
typedef struct {
	FILE *fp;
	char *buf;
	size_t len;
	size_t nrefs;
} String;

typedef struct {
	union {
		/*
		 * This struct is used to reference files/macros in the FILE* stack.
		 * Files still store the FILE* in str->fp so eval() doesn't have to
		 * differentiate between files and macros, but type is FP so we don't
		 * try to free anything. A bit dirty but it makes the code simpler.
		 */
		struct {
			String *str;
			size_t depth;
			fpos_t pos;
		};
		Number num;
		unsigned char reg;
	};
	int type;
} Token;

typedef struct {
	Token *data;
	size_t top;
	size_t cap;
} Stack;

Stack fpstack; /* FILE* stack for macro calls in eval() */
Stack mainstack;
Stack regs[256];

static void vwarn(int err, char *fmt, va_list ap);
static void warn(char *fmt, ...);
static noreturn void die(char *fmt, ...);
static int efgetc(FILE *fp);
static void push(Stack *sp, Token tok);
static Token pop(Stack *sp);
static Token peek(Stack *sp);
static int isempty(Stack *sp);
static void freestack(Stack *sp);
static void cleanup(void);
static Token readstr(FILE *fp);
static Token lex(FILE *fp);
static Token tokdup(Token tok);
static void tokfree(Token tok);
static void arithmetic(Token tok);
static int conditional(Token tok);
static void usereg(Token tok);
static void printtok(Token tok);
static void readuser(void);
static void execute(void);
static int quit(int type);
static int eval(void);

/* Don't worry about checking for errors here, we're already exploding. */
static void
vwarn(int err, char *fmt, va_list ap)
{
	vfprintf(stderr, fmt, ap);

	if (*fmt && fmt[strlen(fmt)-1] == ':')
		fprintf(stderr, " %s", strerror(err));

	fputc('\n', stderr);
}

static void
warn(char *fmt, ...)
{
	int e = errno;
	va_list ap;
	va_start(ap, fmt);
	vwarn(e, fmt, ap);
	va_end(ap);
}

/*
 * Do not call die() from any function run during cleanup() as calling exit()
 * more than once causes undefined behavior.
 */
noreturn static void
die(char *fmt, ...)
{
	int e = errno;
	va_list ap;
	va_start(ap, fmt);
	vwarn(e, fmt, ap);
	va_end(ap);
	exit(1);
}

static int
efgetc(FILE *fp)
{
	int c = fgetc(fp);
	int e = errno;
	if (c == EOF && ferror(fp)) {
		errno = e;
		die("fgetc:");
	}
	return c;
}

static void
push(Stack *sp, Token tok)
{
	if (sp->top == sp->cap) {
		/* If realloc fails sp still has a valid data pointer during cleanup. */
		Stack tmp = *sp;
		tmp.cap = tmp.cap * 2 + 1;
		tmp.data = realloc(tmp.data, sizeof(*tmp.data) * tmp.cap);
		if (!tmp.data)
			die("realloc:");
		*sp = tmp;
	}
	sp->data[sp->top++] = tok;
}

static Token
pop(Stack *sp)
{
	if (isempty(sp))
		die("pop: empty stack");
	return sp->data[--sp->top];
}

static Token
peek(Stack *sp)
{
	if (isempty(sp))
		die("peek: empty stack");
	return sp->data[sp->top - 1];
}

static int
isempty(Stack *sp)
{
	return !sp->top;
}

static void
freestack(Stack *sp)
{
	while (!isempty(sp))
		tokfree(pop(sp));
	free(sp->data);
}

static void
cleanup(void)
{
	for (Stack *sp = regs; sp < regs + sizeof(regs)/sizeof(*regs); sp++)
		freestack(sp);
	freestack(&mainstack);
	freestack(&fpstack);
}

static Token
readstr(FILE *fp)
{
	FILE *stream;
	int c, err, nest = 0;

	String *str = calloc(1, sizeof(*str));
	if (!str)
		die("malloc:");

	stream = open_memstream(&str->buf, &str->len);
	if (!stream) {
		warn("open_memstream:");
		goto freestring;
	}

	/* Don't use efgetc() so we can cleanup on error. */
	while ((c = fgetc(fp)) != EOF) {
		if (c == '[')
			nest++;
		else if (c == ']' && !nest--)
			break;
		if (fputc(c, stream) == EOF)
			break;
	}
	err = errno;

	if (ferror(stream)) {
		errno = err;
		warn("fputc:");
		goto closestream;
	}
	if (fclose(stream)) {
		warn("fclose:");
		goto freestring;
	}
	if (c == EOF) {
		warn("unclosed string: `%s'", str->buf);
		if (ferror(fp)) {
			errno = err;
			warn("fgetc:");
		}
		goto freestring;
	}

	str->fp = fmemopen(str->buf, str->len, "r");
	if (!str->fp) {
		warn("fmemopen:");
		goto freestring;
	}

	str->nrefs = 1;

	return (Token){ .type = STR, .str = str };

closestream:
	if (fclose(stream))
		warn("fclose:");
freestring:
	free(str->buf); /* Valid after successful fclose() or 0 due to calloc() */
	free(str);
	exit(1);
}

/*
 * Tokens cannot span files, don't think that's worth the effort/complexity.
 * Maybe I'll come back to it.
 */
static Token
lex(FILE *fp)
{
	int c;

	while (isspace(c = efgetc(fp)))
		;

	if (c == EOF)
		return (Token){ .type = EOF };

	/* tokens that don't need any special processing */
	if (strchr("+-*/%^pnxfcdrvqQ?", c))
		return (Token){ .type = c };

	/* number */
	if (isdigit(c) || c == '_') {
		int sign = c == '_' ? -1 : 1;

		if (c != '_' && ungetc(c, fp) == EOF)
			die("ungetc:");

		return (Token){ .type = NUM, .num = nummul(numread(fp), sign) };
	}

	/* string */
	if (c == '[')
		return readstr(fp);

	/* store or load registers */
	if (strchr("sSlL", c)) {
		int reg = efgetc(fp);

		if (reg == EOF)
			die("command `%c' missing register", c);
		return (Token){ .type = c, .reg = reg };
	}

	/* conditionals */
	if (strchr("!><=", c)) {
		char cmd[3] = { c };
		int reg;

		if (c == '!') {
			c = efgetc(fp);
			if (c == EOF)
				die("incomplete token `!'");
			cmd[0] = '!';
			cmd[1] = c;
			switch (c) {
				default : die("bad token: `!%c'(%d,%d)", c, '!', c);
				case '>': c = LE; break;
				case '<': c = GE; break;
				case '=': c = NE; break;
			}
		}
		reg = efgetc(fp);
		if (reg == EOF)
			die("command `%s' missing register", cmd);
		return (Token){ .type = c, .reg = reg };
	}

	/* comment */
	if (c == '#') {
		while ((c = efgetc(fp)) != '\n' && c != EOF)
			;
		return (Token){ .type = c == EOF ? EOF : COMMENT };
	}

	die("bad token: `%c'(%d)", c, c);
}

/* Expect more here once aribitrary precision is in place */
static Token
tokdup(Token tok)
{
	if (tok.type == STR)
		++tok.str->nrefs;
	return tok;
}

static void
tokfree(Token tok)
{
	switch (tok.type) {
		case FP:
			if (fclose(tok.str->fp))
				warn("fclose:");
			break;
		case STR:
			if (--tok.str->nrefs)
				break;
			if (fclose(tok.str->fp))
				warn("fclose:");
			free(tok.str->buf);
			free(tok.str);
			break;
	}
}

static void
arithmetic(Token tok)
{
	Token b = pop(&mainstack), a = pop(&mainstack);

	if (a.type != NUM || b.type != NUM)
		die("arithmetic: not a number");

	switch (tok.type) {
		default : die("Impossible token type in %s(): `%c'(%d)", __func__, tok.type, tok.type);
		case '+': push(&mainstack, (Token){ .type = NUM, .num = numadd(a.num, b.num) }); break;
		case '-': push(&mainstack, (Token){ .type = NUM, .num = numsub(a.num, b.num) }); break;
		case '*': push(&mainstack, (Token){ .type = NUM, .num = nummul(a.num, b.num) }); break;
		case '/': push(&mainstack, (Token){ .type = NUM, .num = numdiv(a.num, b.num) }); break;
		case '%': push(&mainstack, (Token){ .type = NUM, .num = nummod(a.num, b.num) }); break;
		case '^': push(&mainstack, (Token){ .type = NUM, .num = numpow(a.num, b.num) }); break;
	}
}

static int
conditional(Token tok)
{
	Token a = pop(&mainstack), b = pop(&mainstack);

	if (a.type != NUM || b.type != NUM)
		die("arithmetic: not a number");

	switch (tok.type) {
		default : die("Impossible token type in %s(): `%c'(%d)", __func__, tok.type, tok.type);
		case '>': return numgt(a.num, b.num);
		case '<': return numlt(a.num, b.num);
		case '=': return numeq(a.num, b.num);
		case GE : return numge(a.num, b.num);
		case LE : return numle(a.num, b.num);
		case NE : return numne(a.num, b.num);
	}
}

/* store and load registers */
static void
usereg(Token tok)
{
	switch (tok.type) {
		default : die("Impossible token type in %s(): `%c'(%d)", __func__, tok.type, tok.type);
		case 's':
			if (!isempty(regs + tok.reg))
				tokfree(pop(regs + tok.reg));
			/* fallthrough */
		case 'S': push(regs + tok.reg, pop(&mainstack)); break;
		case 'L': push(&mainstack, pop(regs + tok.reg)); break;
		case 'l':
			if (isempty(regs + tok.reg))
				tok = (Token){ .type = NUM, .num = 0 };
			else
				tok = tokdup(peek(regs + tok.reg));
			push(&mainstack, tok);
			break;
	}
}

static void
printtok(Token tok)
{
	switch (tok.type) {
		default:
			die("Impossible token type in %s(): `%c'(%d)", __func__, tok.type, tok.type);
		case NUM:
			numprint(tok.num);
			break;
		case STR:
			if (fwrite(tok.str->buf, 1, tok.str->len, stdout) != tok.str->len)
				die("fwrite:");
			break;
	}
}

/* Read a line of input from stdin and push it on the stack */
static void
readuser(void)
{
	String *str = calloc(1,sizeof(*str));
	if (!str)
		die("malloc:");

	ssize_t len = getline(&str->buf, &str->len, stdin);
	if (len == EOF) {
		free(str);
		die("getline:");
	}
	str->nrefs = 1;
	str->len = len;
	str->fp = fmemopen(str->buf, str->len, "r");
	if (!str->fp) {
		free(str->buf);
		free(str);
		die("fmemopen:");
	}
	push(&mainstack, (Token){ .type = STR, .str = str });
}

/*
 * Execute the top of the stack.
 *
 * Tail call elimination:
 *
 * If we are at the end of our current stream, pop it before pushing the new one
 * so the FILE* stack doesn't overflow, but increment the depth so we can keep
 * track of how many calls we are unwinding for q and Q.
 *
 * If we are not at the end of the current stream, store the position. As the
 * stream is part of the referenced count string the position will change if the
 * same string is being executed elsewhere in the call stack and we need to pick
 * up where we left off when we come back to it.
 *
 * Either way, rewind the stream we are about to execute to make sure we start
 * from the beginning.
 *
 * Use fgetc() instead of calling lex() like I should so that I can use ungetc()
 * instead of dealing with writing some sort of unlex() to buffer the token. We
 * don't care about whitespace so eating that is fine.
 *
 */
static void
execute(void)
{
	if (peek(&mainstack).type != STR)
		return;

	Token tok;
	int depth, c;
	FILE *fp = peek(&fpstack).str->fp;

	/* Special case for stdin for interactive use to execute immediately. */
	while (isspace(c = efgetc(fp)) && fp != stdin)
		;

	if (c == EOF) {
		depth = peek(&fpstack).depth + 1;
		tokfree(pop(&fpstack));
	} else {
		depth = 0;
		if (ungetc(c, fp) == EOF)
			die("ungetc:");

		/*
		 * Only get/set position for macros, not real files.
		 * peek/pop/push dance so we still free if fgetpos fails.
		 */
		tok = peek(&fpstack);
		if (tok.type == STR && fgetpos(fp, &tok.pos))
			die("fgetpos:");
		pop(&fpstack);
		push(&fpstack, tok);
	}

	tok = pop(&mainstack);
	tok.depth = depth;
	rewind(tok.str->fp);

	push(&fpstack, tok);
}

/*
 * Quit the current macro and/or dc
 * return 1 to quit dc, 0 otherwise
 *
 * Unwinding tail calls: If the requested depth is greater than the depth
 * stored for number of tail calls, we subtract, pop, and loop again.
 * If the requested depth is less than the depth stored, we subtract, pop,
 * but don't loop because the remainin depth is negative.
 *
 * Explanation: The first level is the currently executing macro, we know
 * for sure we are exiting that.  Every layer below that represented in
 * the stored depth is a macro already at EOF.  If it weren't at EOF it
 * would be its own entry on the stack, not part of the stored depth.
 * Say we had stored depth 5 and tried to Q out of 2, the next three are
 * already at EOF and would exit immediately.  At that point we would pop
 * the top of the FILE* stack and start reading at the next one.  So pop
 * that stack now instead of getting stuck trying to deal with how many
 * useless layers are left.
 */
static int
quit(int type)
{
	Token tok;
	int depth = 2;

	if (type == 'Q') {
		tok = peek(&mainstack);
		if (tok.type != NUM)
			die("Q: not a number");
		if (tok.num < 1)
			die("Q: invalid number %f < 1", tok.num);
		pop(&mainstack); /* XXX: tokfree() */
		depth = tok.num;
	}

	while (depth > 0) {
		tok = pop(&fpstack);
		depth -= tok.depth + 1;

		if (!isempty(&fpstack)) {
			tokfree(tok);
			continue;
		}

		/*
		 * 'Q' cannot quit dc.
		 * If the last entry in the FILE* stack has depth 0 then it is the real
		 * file and Q cannot consume it. If it is nonzero then we already
		 * reached EOF on the real file and removed it in tail call elimination
		 * so it's safe to consume this last entry.
		 */
		if (!tok.depth && type == 'Q') {
			push(&fpstack, tok);
			return 0;
		}

		tokfree(tok);
		return type == 'q';
	}

	/*
	 * FILE* stack is not empty and we are not exiting. If the top of the FILE*
	 * stack is a macro, make sure we pick up where we left off.
	 */
	tok = peek(&fpstack);
	if (tok.type == STR && fsetpos(tok.str->fp, &tok.pos))
		die("fsetpos:");

	return 0;
}

static int
eval(void)
{
	int halt = 0;

	while (!isempty(&fpstack) && !halt) {
		Token tok = lex(peek(&fpstack).str->fp);

		switch (tok.type) {
			default :
				die("Impossible token type in %s(): `%c'(%d)", __func__, tok.type, tok.type);

			case EOF:
				tokfree(pop(&fpstack));
				if (!isempty(&fpstack)) {
					tok = peek(&fpstack);
					/* Set stream position for macros, same as in quit() */
					if (tok.type == STR && fsetpos(tok.str->fp, &tok.pos))
						die("fsetpos:");
				}
				break;

			case COMMENT:
				break;

			case STR:
			case NUM:
				push(&mainstack, tok);
				break;

			case '+':
			case '-':
			case '*':
			case '/':
			case '%':
			case '^':
				arithmetic(tok);
				break;

			case 'v':
				if (peek(&mainstack).type != NUM)
					die("v: not a number");
				push(&mainstack, (Token){ .type = NUM, .num = numsqrt(pop(&mainstack).num) });
				break;

			case 'r':
			{
				Token tmp = pop(&mainstack);
				tok = pop(&mainstack);
				push(&mainstack, tmp);
				push(&mainstack, tok);
				break;
			}

			case 'd':
				push(&mainstack, tokdup(peek(&mainstack)));
				break;

			case 'c':
				freestack(&mainstack);
				break;

			case 'f':
				for (Token *tp = mainstack.data + mainstack.top - 1; tp >= mainstack.data; tp--) {
					printtok(*tp);
					if (putchar('\n') == EOF)
						die("putchar:");
				}
				break;

			case 'p':
				printtok(peek(&mainstack));
				if (putchar('\n') == EOF)
					die("putchar:");
				break;

			case 'n':
				printtok(peek(&mainstack));
				tokfree(pop(&mainstack));
				if (fflush(stdout) == EOF)
					die("fflush:");
				break;

			case 's':
			case 'S':
			case 'l':
			case 'L':
				usereg(tok);
				break;

			case '>':
			case '<':
			case '=':
			case GE :
			case LE :
			case NE :
				if (!conditional(tok))
					break;
				push(&mainstack, tokdup(peek(regs + tok.reg)));
				execute();
				break;

			case '?':
				readuser();
				execute();
				break;

			case 'x':
				execute();
				break;

			case 'q':
			case 'Q':
				halt = quit(tok.type);
				break;
		}
	}

	if (!isempty(&fpstack))
		die("Impossible FILE* stack not empty after %s()", __func__);
	return halt;
}

int
main(int argc, char *argv[])
{
	if (atexit(cleanup))
		die("atexit:");

	while (*++argv) {
		/*
		 * fp is fclose()ed in tokfree() from either
		 * 1) the EOF case in eval()
		 * 2) quit()
		 * 3) tail call elimination in execute()
		 */
		FILE *fp = fopen(*argv, "r");

		if (!fp)
			die("fopen:");

		push(&fpstack, (Token){ .type = FP, .str = &(String){ .fp = fp } });

		if (eval())
			return 0;
	}

	push(&fpstack, (Token){ .type = FP, .str = &(String){ .fp = stdin } });
	eval();

	return 0;
}
