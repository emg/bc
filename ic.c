/*
 * ic: interactive calculator
 *
 * ic is an interactive RPN calculator with fixed stack size. As it is meant for
 * interactive use, it reads keys as they are pressed instead of waiting for a
 * newline and instantly updates the stack. It behaves like HP's RPN
 * calculators. It's designed for use with the numpad. More functions are
 * available by putting the left hand on home row.
 *
 * If an argument is provided to ic it is used as the stack size. The stack size
 * defaults to 4.
 *
 * Digits, decimal point, arithmetic characters, enter, backspace, all work as
 * expected.
 *
 * The top row of characters is the same for every mode to allow basic RPN
 * manipulations in any mode.
 *
 * Use of a function will return to normal mode (just like HP calculators).
 * Escape will return to normal mode.
 *
 * Normal Mode Keys
 * ----------------
 * [q]:<--  [w]:Swap [e]:Rot  [r]:Chs   [t]:EEX
 *  [a]:Add  [s]:Sub  [d]:Mul  [f]:Div   [g]:Sqrt
 *   [z]:Trig [x]:Hyp  [c]:Exp  [v]:Const [b]:Other
 *                   [space]:
 *
 * Trig Mode Keys
 * --------------
 * [q]:<--  [w]:Swap [e]:Rot  [r]:Chs  [t]:EEX
 *  [a]:2Deg [s]:Sin  [d]:Cos  [f]:Tan  [g]: Degrees
 *   [z]:2Rad [x]:aSin [c]:aCos [v]:aTan [b]: Radians
 *                   [space]:
 *
 * Hyperbolic Mode Keys
 * ------------------
 * [q]:<--  [w]:Swap  [e]:Rot   [r]:Chs   [t]:EEX
 *  [a]:     [s]:Sinh  [d]:Cosh  [f]:Tanh  [g]:
 *   [z]:     [x]:aSinh [c]:aCosh [v]:aTanh [b]:
 *                   [space]:
 *
 * Exponent Mode Keys
 * ------------------
 * [q]:<--  [w]:Swap [e]:Rot  [r]:Chs  [t]:EEX
 *  [a]:e^x  [s]:10^x [d]:2^x  [f]:y^x  [g]:Sqrt
 *   [z]:Ln   [x]:Log  [c]:Log2 [v]:Logx [b]:Cbrt
 *                   [space]:
 *
 * Constant Mode Keys
 * ------------------
 * [q]:<--  [w]:Swap [e]:Rot  [r]:Chs  [t]:EEX
 *  [a]:e    [s]:Pi   [d]:     [f]:     [g]:
 *   [z]:     [x]:     [c]:     [v]:     [b]:
 *                   [space]:
 *
 * Other Mode Keys
 * ------------------
 * [q]:<--  [w]:Swap [e]:Rot  [r]:Chs  [t]:EEX
 *  [a]:x^2  [s]:%    [d]:Int  [f]:Rnd  [g]:Cy,x
 *   [z]:1/x  [x]:%ch  [c]:Frac [v]:Fact [b]:Py,x
 *                   [space]:
 *
 * Full Mode
 * ---------
 * [q]:     [w]:     [e]:     [r]:     [t]:     [y]:     [u]:     [i]:     [o]:     [p]:     [[]:     []]:     [\]:
 *  [a]:     [s]:     [d]:     [f]:     [g]:     [h]:     [j]:     [k]:     [l]:     [;]:     [']:
 *   [z]:     [x]:     [c]:     [v]:     [b]:     [n]:     [m]:     [,]:     [.]:     [/]:
 *
 * [Q]:     [W]:     [E]:     [R]:     [T]:     [Y]:     [U]:     [I]:     [O]:     [P]:     [{]:     [}]:     [|]:
 *  [A]:     [S]:     [D]:     [F]:     [G]:     [H]:     [J]:     [K]:     [L]:     [:]:     [""]:
 *   [Z]:     [X]:     [C]:     [V]:     [B]:     [N]:     [M]:     [<]:     [>]:     [?]:
 *
 *
 * TODO
 * reorder functions to something that makes more sense
 * bitwise operations?
 * probably change asdfg in left hand normal mode to more modes, they're available on numpad
 * Num type: Dbl, Flt, Arb, SN, UN (Signed and Unsigned N in 8, 16, 32, 64)
 * Base: Dec, Hex, Oct (perhaps just print base in base10 in future? e.g. 8, 10, 16, 124...)
 * Trig mode: Deg, Rad, Grd
 *
 * Status flags on left side: default Deg, Dec, Dbl
 *
 * Deg | [q]:     [w]:     [e]:     [r]:     [t]:
 * Dec |  [a]:     [s]:     [d]:     [f]:     [g]:
 * Dbl |   [z]:     [x]:     [c]:     [v]:     [b]:
 */

#include <ctype.h>
#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdnoreturn.h>
#include <string.h>
#include <termios.h>

enum { DEG, RAD, GRD };

typedef union Arg Arg;
typedef struct Key Key;

union Arg {
	void (*func)(void);
	Key *map;
	double num;
	char c;
};

struct Key {
	char c;
	void (*func)(Arg arg);
	Arg arg;
	char *name;
	char *description;
};

static void opadd(void);
static void opsub(void);
static void opmul(void);
static void opdiv(void);

static void optrig(double (*func)(double));

static void opsin  (void);
static void opcos  (void);
static void optan  (void);
static void opasin (void);
static void opacos (void);
static void opatan (void);
static void opsinh (void);
static void opcosh (void);
static void optanh (void);
static void opasinh(void);
static void opacosh(void);
static void opatanh(void);

static void opradtodeg(void);
static void opdegtorad(void);
static void opradtogrd(void);
static void opgrdtorad(void);
static void opdegtogrd(void);
static void opgrdtodeg(void);

static void opsqrt  (void);
static void opcbrt  (void);
static void opln    (void);
static void oplog   (void);
static void oplog2  (void);
static void opexp   (void);
static void opexp10 (void);
static void opsquare(void);
static void oprecip (void);
static void oplogx  (void);
static void oppow   (void);

static void oppercent(void);
static void opprcntch(void);

static void opint  (void);
static void opfrac (void);
static void opround(void);

static void opfact  (void);
static void opchoose(void);
static void opperms (void);

static void endinput(void);
static void noendin(Arg arg);
static void stackop(Arg arg);
static void nodrop (Arg arg);
static void dodrop (Arg arg);

static void putnum   (Arg arg);
static void switchmap(Arg arg);
static void settrig  (Arg arg);

static void enter(void);
static void swp  (void);
static void rot  (void);
static void bsp  (void);
static void chs  (void);
static void eex  (void);
static void help (void);
static void quit (void);

static void input(Arg arg);

static noreturn void die(char *fmt, ...);
static void cleanup(void);
static void maybelift(void);
static void lift(void);
static void drop(void);
static void intox(void);
static void draw(void);
static void run(void);
static void init(void);

#include "icconfig.h"

struct termios *termp, oldterm;

double *stack;
size_t stacksize = 4;
int trigmode = DEG;
char numbuf[32], *nump;
int innum, inexp, infrac, sign = 1;
Key *last = &(Key){ '\n', stackop, { .func = enter }, 0, 0 };
Key *normmap;

/* Arithmetic */
static void opadd(void) { stack[1] += *stack; }
static void opsub(void) { stack[1] -= *stack; }
static void opmul(void) { stack[1] *= *stack; }
static void opdiv(void) { stack[1] /= *stack; }

/* Trig */
static void
optrig(double (*func)(double)) {
	switch (trigmode) {
		case DEG: opdegtorad(); break;
		case GRD: opgrdtorad(); break;
		case RAD: break;
	}
	*stack = func(*stack);
}
static void opsin (void) { optrig(sin ); }
static void opcos (void) { optrig(cos ); }
static void optan (void) { optrig(tan ); }
static void opasin(void) { optrig(asin); }
static void opacos(void) { optrig(acos); }
static void opatan(void) { optrig(atan); }

static void opsinh (void) { *stack = sinh (*stack);}
static void opcosh (void) { *stack = cosh (*stack);}
static void optanh (void) { *stack = tanh (*stack);}
static void opasinh(void) { *stack = asinh(*stack);}
static void opacosh(void) { *stack = acosh(*stack);}
static void opatanh(void) { *stack = atanh(*stack);}

static void opradtodeg(void) { *stack = *stack * 180 / M_PI; }
static void opdegtorad(void) { *stack = *stack * M_PI / 180; }
static void opradtogrd(void) { *stack = *stack * 200 / M_PI; }
static void opgrdtorad(void) { *stack = *stack * M_PI / 200; }
static void opdegtogrd(void) { *stack = *stack * 10 / 9; }
static void opgrdtodeg(void) { *stack = *stack * 9 / 10; }

/* Exponents */
static void opsqrt  (void) { *stack = sqrt (*stack); }
static void opcbrt  (void) { *stack = cbrt (*stack); }
static void opln    (void) { *stack = log  (*stack); }
static void oplog   (void) { *stack = log10(*stack); }
static void oplog2  (void) { *stack = log2 (*stack); }
static void opexp   (void) { *stack = exp  (*stack); }
static void opexp10 (void) { *stack = pow(10, *stack); }
static void opsquare(void) { *stack *= *stack; }
static void oprecip (void) { *stack = 1 / *stack; }

static void oplogx  (void) { stack[1] = log(stack[1]) / log(*stack); }
static void oppow   (void) { stack[1] = pow(stack[1], *stack); }

static void oppercent(void) { *stack = *stack / 100 * stack[1]; }
static void opprcntch(void) { *stack = (*stack - stack[1]) / stack[1] * 100; }

static void opint  (void) { *stack = trunc(*stack); }
static void opfrac (void) { *stack = fmod(*stack, 1); }
static void opround(void) { *stack = round(*stack); }

static void opfact  (void) { *stack = tgamma(*stack + 1); }
static void opchoose(void) { stack[1] = tgamma(stack[1] + 1) / (tgamma(*stack + 1) * tgamma(stack[1] - *stack + 1)); }
static void opperms (void) { stack[1] = tgamma(stack[1] + 1) / tgamma(stack[1] - *stack + 1); }

static void endinput(void) { if (innum) intox(); }

static void noendin(Arg arg) { arg.func(); }
static void stackop(Arg arg) { endinput(); arg.func(); }
static void nodrop (Arg arg) { endinput(); arg.func(); curmap = normmap; }
static void dodrop (Arg arg) { endinput(); arg.func(); curmap = normmap; drop(); }
static void putnum (Arg arg) { endinput(); maybelift(); *stack = arg.num; curmap = normmap; }

static void switchmap(Arg arg) { curmap = arg.map; }
static void settrig  (Arg arg) { trigmode = arg.c; }

static void
enter(void)
{
	endinput();
	lift();
	*stack = stack[1];
}

static void
swp(void)
{
	double tmp = *stack;
	*stack = stack[1];
	stack[1] = tmp;
}

static void
rot(void)
{
	double tmp = *stack;
	drop();
	stack[stacksize-1] = tmp;
}

/* Backspace */
static void
bsp(void)
{
	if (!innum) {
		*stack = 0;
		nump = numbuf;
		return;
	}
	if (inexp && nump == numbuf) {
		if (sign < 0) {
			sign = 1;
			return;
		}
		if (*stack < 0) {
			sign = -1;
			*stack *= -1;
		} else {
			sign = 1;
		}

		int len = snprintf(numbuf, sizeof(numbuf), "%G", *stack);
		if (len < 0)
			die("snprintf:");
		nump = numbuf + len;
		inexp = 0;
		return;
	}
	if (nump > numbuf) {
		if (nump[-1] == '.')
			infrac = 0;
		*--nump = 0;
	}
	if (nump == numbuf && !inexp) {
		*stack = 0;
		innum = 0;
	}
}

/* Change sign */
static void
chs(void)
{
	if (innum)
		sign *= -1;
	else
		*stack *= -1;
}

/* Enter exponent */
static void
eex(void)
{
	if (innum) {
		intox();
	} else {
		maybelift();
		*stack = 1;
	}
	inexp = 1;
	innum = 1;
	sign = 1;
}

static void
quit(void)
{
	exit(0);
}

static void
help(void)
{
	printf("\033[%zuA\r", stacksize+3); /* TODO: magic number */

	for (Key *kp = curmap; kp->c; kp++) {
		if (kp->c == '\n')
			continue;
		printf("[%c]: %s\033[K\n\r", kp->c, kp->description);
	}
	for (int i = stacksize + 4; i; i--) /* TODO: magic number */
		putchar('\n');
}

static void
input(Arg arg)
{
	char c = arg.c;
	if (c == '.') {
		if (infrac)
			return;
		infrac = 1;
	}
	if (!innum) {
		innum = 1;
		sign = 1;
		maybelift();
	}
	if (nump < numbuf + sizeof(numbuf)/sizeof(*numbuf) - 1){ /* 1 byte to terminate */
		*nump++ = c;
		*nump = 0;
	}
}

static noreturn void
die(char *fmt, ...)
{
	int e = errno;

	va_list ap;
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (*fmt && fmt[strlen(fmt)-1] == ':')
		fprintf(stderr, " %s", strerror(e));

	fputc('\n', stderr);
	exit(1);
}

static void
cleanup(void)
{
	if (termp)
		tcsetattr(0, TCSANOW, termp);
	putchar('\n');
}

/*
 * if the user just hit enter then starts typing a number don't lift
 * if the user just used backspace to clear x or to delete the last digit there, don't lift
 */
static void
maybelift(void)
{
	if ((last->func == stackop && last->arg.func == enter) ||
	    (last->func == noendin && last->arg.func == bsp))
		return;
	lift();
}

static void
lift(void)
{
	for (double *p = stack + stacksize - 1; p > stack; p--)
		*p = p[-1];
}

static void
drop(void)
{
	for (double *p = stack; p < stack + stacksize - 1; p++)
		*p = p[1];
}

/* put the input buffer into the x register */
static void
intox(void)
{
	if (inexp) {
		*stack = *stack * pow(10, sign * atof(numbuf));
	} else if (innum) {
		*stack = sign * atof(numbuf);
	} else {
		die("called intox when not innum");
	}
	innum = 0;
	inexp = 0;
	infrac = 0;
	nump = numbuf;
	*nump = 0;
}

/* TODO: status */
static void
draw(void)
{
	printf("\033[%zuA\r", stacksize+3); /* TODO: magic number */

	int i = 0;
	for (Key *kp = curmap; kp->c; kp++) {
		if (kp->c == '\n')
			printf("\033[K\n\r%*s", ++i, "");
		else
			printf("[%c]:%-6s", kp->c, kp->name ? kp->name : ""); /* TODO: magic number */
	}
	fputs("\r\n", stdout);

	for (double *p = stack + stacksize - 1; p > stack; p--)
		printf("%G\033[K\r\n", *p);

	if (inexp)
		printf("%GE", *stack);

	if (innum) {
		if (sign < 0)
			putchar('-');
		fputs(numbuf, stdout);
	} else {
		if (*stack == 0)
			*stack = 0;
		printf("%G", *stack);
	}

	fputs("\033[K", stdout);
}

static void
run(void)
{
	int c = 0;
	for (;;) {
		draw();
		c = getchar();

		/* let curmap override univmap */
		Key *maps[] = { curmap, univmap, 0 };
		for (Key **map = maps; *map; map++) {
			for (Key *kp = *map; kp->c; kp++) {
				if (c == kp->c && kp->func) {
					kp->func(kp->arg);
					last = kp;
					goto break2;
				}
			}
		}
		break2:;
	}
}

static void
init(void)
{
	if (atexit(cleanup))
		die("atexit:");

	if (setvbuf(stdin, 0, _IONBF, 0))
		die("setvbuf:");

	if (setvbuf(stdout, 0, _IONBF, 0))
		die("setvbuf:");

	if (tcgetattr(0, &oldterm))
		die("tcgetattr:");

	termp = &oldterm;

	struct termios new = oldterm;
	new.c_lflag &= ~ICANON;
	new.c_lflag &= ~IGNCR;
	new.c_lflag &= ~ECHO;

	new.c_cc[VMIN ] = 1;
	new.c_cc[VTIME] = 0;

	if (tcsetattr(0, TCSANOW, &new))
		die("tcsetattr:");

	nump = numbuf;
	for (double *p = stack; p < stack + stacksize; p++)
		*p = 0;

	printf("?: help\n\r");
	printf("esc: return to normal mode\n\r");
	printf("numpad and backspace work as expected\n\r\n\r");
	for (int i = stacksize + 3; i; i--)
		putchar('\n');

	normmap = curmap;
}

int
main(int argc, char *argv[])
{
	if (argc == 2)
		stacksize = atoi(argv[1]);

	double stackbuf[stacksize];
	stack = stackbuf;

	init();
	run();

	return 0;
}
