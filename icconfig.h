/*
 * intptr_t, void*, and function pointers (this is ugly)
 *
 * ISO C requires conversion from void* to intptr_t and back compare equal
 * POSIX requires conversion from void* to function pointer to work
 * No gaurantees about conversion from function pointer to void*
 * No guarantees about conversion between intptr_t and function pointers
 * No guarantees about intptr_t to void* and back to intptr_t
 *
 * Screw that, I'm casting from function pointer to intptr_t and back.
 * I'll fix this all up with unions or some such later. I'm having too much fun
 * right now to worry about it.
 */

Key othermap[] = {
	{ 'q', noendin, { .func = bsp }, "<--" , "Backspace"      },
	{ 'w', stackop, { .func = swp }, "Swap", "Swap x and y"   },
	{ 'e', stackop, { .func = rot }, "Rot" , "Rotate stack"   },
	{ 'r', noendin, { .func = chs }, "Chs" , "Change sign"    },
	{ 't', noendin, { .func = eex }, "EEX" , "Enter exponent" },
	{ .c = '\n' },

	{ 'a', nodrop, { .func = opsquare  }, "x^2" , "Square"               },
	{ 's', nodrop, { .func = oppercent }, "%"   , "Percentage"           },
	{ 'd', nodrop, { .func = opint     }, "Int" , "Integer part"         },
	{ 'f', nodrop, { .func = opround   }, "Rnd" , "Round"                },
	{ 'g', dodrop, { .func = opchoose  }, "Cy,x", "Binomial Coefficient" },
	{ .c = '\n' },

	{ 'z', nodrop, { .func = oprecip   }, "1/x" , "Reciprocal"      },
	{ 'x', nodrop, { .func = opprcntch }, "%ch" , "Percent change"  },
	{ 'c', nodrop, { .func = opfrac    }, "Frac", "Fractional part" },
	{ 'v', nodrop, { .func = opfact    }, "Fact", "Factorial"       },
	{ 'b', dodrop, { .func = opperms   }, "Py,x", "Permutations"    },
	{ .c = '\n' },

	{ .c = 0 }
};

Key constmap[] = {
	{ 'q', noendin, { .func = bsp }, "<--" , "Backspace"      },
	{ 'w', stackop, { .func = swp }, "Swap", "Swap x and y"   },
	{ 'e', stackop, { .func = rot }, "Rot" , "Rotate stack"   },
	{ 'r', noendin, { .func = chs }, "Chs" , "Change sign"    },
	{ 't', noendin, { .func = eex }, "EEX" , "Enter exponent" },
	{ .c = '\n' },

	{ 'a', putnum, { .num = M_PI }, "Pi", "Pi"             },
	{ 's', putnum, { .num = M_E  }, "e" , "Euler's Number" },
	{ .c = 'd' },
	{ .c = 'f' },
	{ .c = 'g' },
	{ .c = '\n' },

	{ .c = 'z' },
	{ .c = 'x' },
	{ .c = 'c' },
	{ .c = 'v' },
	{ .c = 'b' },
	{ .c = '\n' },

	{ .c = 0 }
};

Key trigmap[] = {
	{ 'q', noendin, { .func = bsp }, "<--" , "Backspace"      },
	{ 'w', stackop, { .func = swp }, "Swap", "Swap x and y"   },
	{ 'e', stackop, { .func = rot }, "Rot" , "Rotate stack"   },
	{ 'r', noendin, { .func = chs }, "Chs" , "Change sign"    },
	{ 't', noendin, { .func = eex }, "EEX" , "Enter exponent" },
	{ .c = '\n' },

	{ 'a', nodrop , { .func = opradtodeg }, "2Deg"   , "Radians -> Degrees" },
	{ 's', nodrop , { .func = opsin      }, "Sin"    , "Sine"               },
	{ 'd', nodrop , { .func = opcos      }, "Cos"    , "Cosine"             },
	{ 'f', nodrop , { .func = optan      }, "Tan"    , "Tangent"            },
	{ 'g', settrig, { .c    = DEG        }, "Degrees", "Degrees mode"       },
	{ .c = '\n' },

	{ 'z', nodrop , { .func = opdegtorad }, "2Rad"   , "Degrees -> Radians" },
	{ 'x', nodrop , { .func = opasin     }, "aSin"   , "Arcsine"            },
	{ 'c', nodrop , { .func = opacos     }, "aCos"   , "Arccosine"          },
	{ 'v', nodrop , { .func = opatan     }, "aTan"   , "Arctangent"         },
	{ 'b', settrig, { .c    = RAD        }, "Radians", "Radians mode"       },
	{ .c = '\n' },

	{ .c = 0 }
};

Key hypmap[] = {
	{ 'q', noendin, { .func = bsp }, "<--" , "Backspace"      },
	{ 'w', stackop, { .func = swp }, "Swap", "Swap x and y"   },
	{ 'e', stackop, { .func = rot }, "Rot" , "Rotate stack"   },
	{ 'r', noendin, { .func = chs }, "Chs" , "Change sign"    },
	{ 't', noendin, { .func = eex }, "EEX" , "Enter exponent" },
	{ .c = '\n' },

	{ .c = 'a' },
	{ 's', nodrop, { .func = opsinh }, "Sinh", "Hyperbolic sine"    },
	{ 'd', nodrop, { .func = opcosh }, "Cosh", "Hyperbolic cosine"  },
	{ 'f', nodrop, { .func = optanh }, "Tanh", "Hyperbolic tangent" },
	{ .c = 'g' },
	{ .c = '\n' },

	{ .c = 'z' },
	{ 'x', nodrop, { .func = opasinh }, "aSinh", "Inverse hyperbolic sine"    },
	{ 'c', nodrop, { .func = opacosh }, "aCosh", "Inverse hyperbolic cosine"  },
	{ 'v', nodrop, { .func = opatanh }, "aTanh", "Inverse hyperbolic tangent" },
	{ .c = 'b' },
	{ .c = '\n' },

	{ .c = 0 }
};

Key expmap[] = {
	{ 'q', noendin, { .func = bsp }, "<--" , "Backspace"      },
	{ 'w', stackop, { .func = swp }, "Swap", "Swap x and y"   },
	{ 'e', stackop, { .func = rot }, "Rot" , "Rotate stack"   },
	{ 'r', noendin, { .func = chs }, "Chs" , "Change sign"    },
	{ 't', noendin, { .func = eex }, "EEX" , "Enter exponent" },
	{ .c = '\n' },

	{ 'a', nodrop, { .func = opexp   }, "e^x" , "Natural exponential function" },
	{ 's', nodrop, { .func = opexp10 }, "10^x", "Power of 10"                  },
	{ 'd', nodrop, { .func = opexp10 }, "2^x" , "Power of 2"                   },
	{ 'f', dodrop, { .func = oppow   }, "y^x" , "Exponentiation"               },
	{ 'g', nodrop, { .func = opsqrt  }, "Sqrt", "Square root"                  },
	{ .c = '\n' },

	{ 'z', nodrop, { .func = opln   }, "Ln"  , "Natural log"    },
	{ 'x', nodrop, { .func = oplog  }, "Log" , "Log base 10"    },
	{ 'c', nodrop, { .func = oplog2 }, "Log2", "Log base 2"     },
	{ 'v', dodrop, { .func = oplogx }, "Logx", "Log base x of y"},
	{ 'b', nodrop, { .func = opcbrt }, "Cbrt", "Cube root"      },
	{ .c = '\n' },

	{ .c = 0 }
};

Key leftnormmap[] = {
	{ 'q', noendin, { .func = bsp }, "<--" , "Backspace"      },
	{ 'w', stackop, { .func = swp }, "Swap", "Swap x and y"   },
	{ 'e', stackop, { .func = rot }, "Rot" , "Rotate stack"   },
	{ 'r', noendin, { .func = chs }, "Chs" , "Change sign"    },
	{ 't', noendin, { .func = eex }, "EEX" , "Enter exponent" },
	{ .c = '\n' },

	{ 'a', dodrop, { .func = opadd  }, "Add" , "Add"         },
	{ 's', dodrop, { .func = opsub  }, "Sub" , "Subtract"    },
	{ 'd', dodrop, { .func = opmul  }, "Mul" , "Multiply"    },
	{ 'f', dodrop, { .func = opdiv  }, "Div" , "Divide"      },
	{ 'g', nodrop, { .func = opsqrt }, "Sqrt", "Square root" },
	{ .c = '\n' },

	{ 'z', switchmap, { .map = trigmap  }, "Trig" , "Trig mode"       },
	{ 'x', switchmap, { .map = hypmap   }, "Hyp"  , "Hyperbolic mode" },
	{ 'c', switchmap, { .map = expmap   }, "Exp"  , "Exponent mode"   },
	{ 'v', switchmap, { .map = constmap }, "Const", "Constants mode"  },
	{ 'b', switchmap, { .map = othermap }, "Other", "Other mode"      },
	{ .c = '\n' },

	{ .c = 0 }
};

/*
 * Universal key map, keys that are always available unless overriden by
 * current map
 * Include []\ for rotate, swap, change sign during single hand use.
 */
Key univmap[] = {
	{ '0', input, { .c = '0' }, 0, 0 },
	{ '1', input, { .c = '1' }, 0, 0 },
	{ '2', input, { .c = '2' }, 0, 0 },
	{ '3', input, { .c = '3' }, 0, 0 },
	{ '4', input, { .c = '4' }, 0, 0 },
	{ '5', input, { .c = '5' }, 0, 0 },
	{ '6', input, { .c = '6' }, 0, 0 },
	{ '7', input, { .c = '7' }, 0, 0 },
	{ '8', input, { .c = '8' }, 0, 0 },
	{ '9', input, { .c = '9' }, 0, 0 },
	{ '.', input, { .c = '.' }, 0, 0 },

	{ '\n', stackop, { .func = enter }, 0, 0 },
	{ '\r', stackop, { .func = enter }, 0, 0 },

	{ '\b', noendin, { .func = bsp }, 0, 0 },
	{ 127 , noendin, { .func = bsp }, 0, 0 },

	{ '+', dodrop, { .func = opadd }, 0, 0 },
	{ '-', dodrop, { .func = opsub }, 0, 0 },
	{ '*', dodrop, { .func = opmul }, 0, 0 },
	{ '/', dodrop, { .func = opdiv }, 0, 0 },

	{ 27, switchmap, { .map = leftnormmap }, 0, 0 },

	{ 4, nodrop, { .func = quit }, 0, 0 },

	{ '[' , nodrop, { .func = rot }, 0, 0 },
	{ ']' , nodrop, { .func = swp }, 0, 0 },
	{ '\\', noendin, { .func = chs }, 0, 0 },

	{ '?', noendin, { .func = help }, 0, 0 },

	{ .c = 0 }
};

Key *curmap = leftnormmap;
