.POSIX:
.SUFFIXES:
.SUFFIXES: .c

CC = musl-clang
CFLAGS = -O3 -march=native -std=c11 -pedantic -Wall -Wextra -D_XOPEN_SOURCE=700
LDFLAGS = -s -static
libs = -lm

#CC = gcc
#CFLAGS = -g -Og -std=c11 -pedantic -Wall -Wextra -D_XOPEN_SOURCE=700
#LDFLAGS =
#libs = -lm

.c:
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $< $(libs)

all: dc ic

ic: icconfig.h

clean:
	rm -f dc ic
